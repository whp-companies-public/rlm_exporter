# Description

This is a simple prometheus exporter for collecting usage stats of licenses on an RLM License server. It connects directly to the license server and does not require `rlmutil` installed.

# Dependencies

Python 3
prometheus_client (python module)

# Usage

```bash
rlm_exporter.py conf/example.json
```

# docker
be sure to build the container against the platform on which you intend it to run, this uses rlm_exporter_env.py
```
docker build . -t rlm_exporter
docker run --rm -it -p 9701:9701 -v `pwd`/conf:/conf rlm_exporter /conf/carbon.json
```

# Dashboard

Import the dashboard.json in Grafana

![dashboard](dashboard.png)
