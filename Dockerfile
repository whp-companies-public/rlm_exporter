FROM python:3.11.1
RUN pip install prometheus_client requests
COPY rlm_exporter.py .
ENTRYPOINT ["python","./rlm_exporter.py"]
